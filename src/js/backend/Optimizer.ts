import round from 'lodash/round'
import UserData, {makeAdjustmentRange} from './UserData'
import Step from './Step'
import Node from './Node'

const optimized_min_base = 4

export default class Optimizer {
    number: number
    tree: Array<Array<Node>>
    last: Array<Node>
    bestRow: Array<Node>
    best: Node
    stack: Array<Optimizer>

    constructor(
        public userData: UserData,
        public parent?: Optimizer,
    ) {
        this.number = this.parent ? this.parent.number + 1 : 0
        this.tree = this.computeOptimization()
        this.last = this.tree[this.tree.length - 1]
        this.best = this.computeBest()
        this.bestRow = this.best.stack
        this.stack = [...(this.parent ? this.parent.stack : []), this]
    }

    private computeOptimization() {
        let last: Array<Node>
        return this.userData.steps.map(step => last = this.computeStep(step, last))
    }

    private computeStep(step: Step, last?: Array<Node>) {
        let range = step.range

        if (step === this.userData.steps[this.userData.steps.length - 1])
            range = [this.userData.baseWaterPosition]

        return range.map(x2 => {
            if (!last) return new Node(step, x2, this.userData)
            const nodes = last.map(parent =>
                new Node(step, x2, this.userData, parent))
            return nodes.reduce((min, n) => min.Bsum <= n.Bsum ? min : n)
        })
    }

    private computeBest() {
        return this.last.reduce((min, n) => min.Bsum <= n.Bsum ? min : n)
    }

    public shouldNextOptimizer() {
        if (this.parent) {
            if (
                round(this.best.Bsum, optimized_min_base) ===
                round(this.parent.best.Bsum, optimized_min_base)
            ) return false
        }
        return true
    }

    public nextOptimizer() {
        const steps = this.createOptimizedSteps()
        const userData = new UserData(this.userData.baseWaterPosition, steps)
        return new Optimizer(userData, this)
    }

    public* optimizerIterator() {
        let optimizer: Optimizer = this
        while (optimizer.shouldNextOptimizer()) {
            yield optimizer
            optimizer = optimizer.nextOptimizer()
        }
    }

    private createOptimizedSteps() {
        return this.userData.steps.map((oldStep, i) => {
            const bestNode = this.bestRow[i]
            const delta = oldStep.range[1] - oldStep.range[0]
            const range = makeAdjustmentRange(bestNode.x2 - delta, bestNode.x2 + delta)
            return new Step(oldStep.hour, oldStep.Qpr, oldStep.Pn, range)
        })
    }
}
