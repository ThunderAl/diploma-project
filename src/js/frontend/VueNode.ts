import Vue from 'vue'
import router from './router'
import VueApp from './VueApp.vue'
import './v-util'

Vue.config.productionTip = false

const vue_root = new Vue({
    router,
    render: r => r(VueApp),
})

export default vue_root
