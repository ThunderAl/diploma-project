import VueNode from './frontend/VueNode'

const el = document.createElement('div')
el.id = 'app'
document.body.prepend(el)

VueNode.$mount(el)
