import Optimizer from '../src/js/backend/Optimizer'
import {makeAdjustmentRange, makeUserData} from '../src/js/backend/UserData'
import {StepContent} from '../src/js/backend/Step'

const step_content: Array<StepContent> = [
    {Pn: 2000, Qpr: 3}, {Pn: 2000, Qpr: 4}, {Pn: 2000, Qpr: 4},
    {Pn: 2000, Qpr: 5}, {Pn: 2000, Qpr: 4}, {Pn: 2000, Qpr: 3},
    {Pn: 2400, Qpr: 2}, {Pn: 2400, Qpr: 2}, {Pn: 3000, Qpr: 2},
    {Pn: 3500, Qpr: 2}, {Pn: 3300, Qpr: 3}, {Pn: 3200, Qpr: 3},
    {Pn: 3000, Qpr: 2}, {Pn: 2800, Qpr: 2}, {Pn: 2800, Qpr: 2},
    {Pn: 3000, Qpr: 2}, {Pn: 3000, Qpr: 3}, {Pn: 3100, Qpr: 3.5},
    {Pn: 3300, Qpr: 3.5}, {Pn: 3600, Qpr: 4}, {Pn: 3400, Qpr: 4},
    {Pn: 3000, Qpr: 5}, {Pn: 2500, Qpr: 4}, {Pn: 2200, Qpr: 4},
]

describe('optimizer test', function () {

    let optimizer: Optimizer

    beforeAll(function () {
        const userData = makeUserData(13, makeAdjustmentRange(12.9, 13.1), step_content)
        optimizer = new Optimizer(userData)
    })

    it('optimization check', function () {
        let prev = optimizer
        while (prev.shouldNextOptimizer()) {
            const next = prev.nextOptimizer()
            expect(next.best.Bsum).toBeLessThanOrEqual(prev.best.Bsum)
            prev = next
        }
    })
})
