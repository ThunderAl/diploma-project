import Node from '../src/js/backend/Node'
import Step, {StepContent} from '../src/js/backend/Step'
import UserData, {AdjustmentRange, makeAdjustmentRange, makeUserData} from '../src/js/backend/UserData'
import isArray from 'lodash/isArray'

const content: Array<StepContent> = [
    {Qpr: 3, Pn: 2000},
    {Qpr: 4, Pn: 2000},
    {Qpr: 4, Pn: 2000},
    {Qpr: 5, Pn: 2000},
    {Qpr: 4, Pn: 2000},
    {Qpr: 3, Pn: 2000},
]

describe('testing node subprogram', function () {

    let user_data: UserData
    let range: AdjustmentRange
    let steps: Array<Step>
    let base_node: Node
    let nodes: Array<Node>

    beforeAll(function () {
        range = makeAdjustmentRange(12.9, 13.1)
        user_data = makeUserData(13, range, content)
        steps = user_data.steps
        base_node = new Node(steps[0], range[0], {baseWaterPosition: 13, steps})
        let parent = base_node
        nodes = steps.splice(1).map((s, i) =>
            parent = new Node(s, range[i % range.length], user_data, parent),
        )
    })

    it('testing node without parent', function () {
                expect(base_node.B).toBe(484)
        expect(base_node.Bsum).toBe(484)
    })

    it('testing node with parent', function () {
        nodes.forEach((node: Node) => {
            expect(node.Bsum).toBe(node.B + (node.parent ? node.parent.Bsum : 0))
            expect(isArray(node.stack)).toBeTruthy('no parents stack')
            expect(node.stack[node.stack.length - 1]).toBe(node, 'last stack is this node')
            expect(node.stack[0]).toBe(base_node, 'last stack is base node')
        })
    })
})
