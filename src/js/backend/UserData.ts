import Step, {StepContent} from './Step'

export const range_split_count = 5

export type AdjustmentRange = Array<number>
export type BaseWaterPosition = number

export function makeAdjustmentRange(start: number, end: number) {
    const diff = end - start
    const delta = diff / (range_split_count - 1)
    const range: AdjustmentRange = [start]

    for (let i = 1; i < range_split_count; i++)
        range.push(i * delta + start)

    return range
}

export default class UserData {
    constructor(
        public baseWaterPosition: BaseWaterPosition,
        public steps: Array<Step>,
    ) {
    }
}

export function makeUserData(baseWaterPosition: BaseWaterPosition, range: AdjustmentRange, content: Array<StepContent>) {
    const steps = content.map((c, i) => new Step(c.hour || i, c.Qpr, c.Pn, range))
    return new UserData(baseWaterPosition, steps)
}
