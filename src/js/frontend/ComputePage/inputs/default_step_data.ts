import {StepContent} from '../../../backend/Step'

const step_content: Array<StepContent> = [
    {Pn: 2000, Qpr: 3}, {Pn: 2000, Qpr: 4}, {Pn: 2000, Qpr: 4},
    {Pn: 2000, Qpr: 5}, {Pn: 2000, Qpr: 4}, {Pn: 2000, Qpr: 3},
    {Pn: 2400, Qpr: 2}, {Pn: 2400, Qpr: 2}, {Pn: 3000, Qpr: 2},
    {Pn: 3500, Qpr: 2}, {Pn: 3300, Qpr: 3}, {Pn: 3200, Qpr: 3},
    {Pn: 3000, Qpr: 2}, {Pn: 2800, Qpr: 2}, {Pn: 2800, Qpr: 2},
    {Pn: 3000, Qpr: 2}, {Pn: 3000, Qpr: 3}, {Pn: 3100, Qpr: 3.5},
    {Pn: 3300, Qpr: 3.5}, {Pn: 3600, Qpr: 4}, {Pn: 3400, Qpr: 4},
    {Pn: 3000, Qpr: 5}, {Pn: 2500, Qpr: 4}, {Pn: 2200, Qpr: 4},
]

export default <Array<StepContent>>step_content.map((s, i) => ({...s, hour: i}))
