export default class Step {
    constructor(public hour: number, public Qpr: number, public Pn: number, public range: number[]) {
    }
}

export type StepContent = { hour?: number, Qpr: number, Pn: number }
