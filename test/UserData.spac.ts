import {makeAdjustmentRange, AdjustmentRange, makeUserData} from '../src/js/backend/UserData'

describe('user data test', function () {

    it('adjustment range generator', function () {
        const range = makeAdjustmentRange(5, 9)
        const expects: AdjustmentRange = [5, 6, 7, 8, 9]

        expect(range).toEqual(expects)
    })

    it('making form user data', function () {
        const data = makeUserData(
            13,
            makeAdjustmentRange(12.9, 13.1),
            [
                {Pn: 1, Qpr: 5},
                {Pn: 5, Qpr: 8},
                {Pn: 5, Qpr: 8},
                {Pn: 5, Qpr: 8},
            ],
        )

        expect(data).toBeDefined()
    })
})
