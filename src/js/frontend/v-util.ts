import Vue from 'vue'
import round from 'lodash/round'
import throttle from 'lodash/throttle'

Vue.filter('round', (i: number, precision: number = 2) => round(i, precision))

export function VThrottle(wait: number = 1000, options?: { leading?: boolean, trailing?: boolean }) {
    return function (target: any, propertyName: string, descriptor: any) {
        return {
            configurable: true,
            enumerable: descriptor.enumerable,
            get: function () {
                Object.defineProperty(this, name, {
                    configurable: true,
                    enumerable: descriptor.enumerable,
                    value: throttle(descriptor.value, wait, options),
                })

                return this[name]
            },
        }
    }
}

export const named_colors = {
    blue: '#325D88',
    indigo: '#6610f2',
    purple: '#6f42c1',
    pink: '#e83e8c',
    red: '#d9534f',
    orange: '#F47C3C',
    yellow: '#ffc107',
    green: '#93C54B',
    teal: '#20c997',
    cyan: '#29ABE0',
}

export const colors = Object.values(named_colors)

export function getColorByIndex(i = 0) {
    return colors[i % colors.length]
}
