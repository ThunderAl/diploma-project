import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

const router = new Router({
    mode: 'hash',
    routes: [
        {path: '/', component: require('../StartPage/StartPage.vue').default, name: 'index'},
        {path: '/compute', component: require('../ComputePage/ComputePage.vue').default, name: 'compute'},
    ],
})

export default router
