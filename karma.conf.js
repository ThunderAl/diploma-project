const webpack_config = require('./webpack.config')

module.exports = function (config) {
    config.set({
        basePath: './',
        frameworks: ['jasmine'],
        files: [
            'test/*.ts',
            'test/**/*.ts',
        ],
        preprocessors: {
            'test/*.ts': ['webpack'],
            'test/**/*.ts': ['webpack'],
        },
        browsers: ['PhantomJS'],
        webpack: {
            mode: 'development',
            target: 'node',
            resolve: webpack_config['resolve'],
            module: webpack_config['module'],
        },
        reporters: ['progress'],
        port: 9876,
        colors: true,
        logLevel: config.LOG_INFO,
        autoWatch: true,
        singleRun: false,
        concurrency: Infinity,
    })
}
