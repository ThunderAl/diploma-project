import Step from './Step'
import UserData from './UserData'

export default class Node {
    public stack: Array<Node>
    public x1: number
    public qv: number
    public q: number
    public dh: number
    public Pg: number
    public Pc: number
    public B: number
    public Bsum: number

    constructor(
        public step: Step,
        public x2: number,
        public userData: UserData,
        public parent?: Node,
    ) {
        this.stack = [...(this.parent ? this.parent.stack : []), this]

        this.x1 = this.parent ? this.parent.x2 : userData.baseWaterPosition
        this.qv = this.calcWaterVolumeDiff(this.x1, this.x2)
        this.q = Math.max(0, this.step.Qpr + this.qv)
        this.dh = this.q / 250
        this.Pg = Math.min(1200, this.q * ((this.x1 + this.x2) / 2 - this.dh) * 8)
        this.Pc = this.step.Pn - this.Pg
        this.B = this.calcFuelByPower(this.Pc)
        this.Bsum = this.parent ? this.parent.Bsum + this.B : this.B
    }

    private calcFuelByPower(P: number) {
        return 20 + 0.5 * P + 10 ** -4 * P ** 2
    }

    private calcWaterVolumeDiff(X1: number, X2: number) {
        return (this.calcWaterVolume(X1) - this.calcWaterVolume(X2)) * 278
    }

    private calcWaterVolume(X: number) {
        return 0.2 * X + 0.012 * X ** 2
    }
}
